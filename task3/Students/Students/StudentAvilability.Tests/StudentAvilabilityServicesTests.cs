﻿using StudentsAvilability.Services;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;

namespace StudentAvilability.Tests
{
    [TestClass]
    public class StudentAvilabilityServicesTests
    {
        [TestMethod]
        public void StudentsAvailabilityPerCurrentMonthForAllClassesTest()
        {
            SetValues();

            Assert.IsNotNull(Service.StudentsAvailabilityPerCurrentMonthForAllClasses());
        }

        [TestMethod]
        public void StudentsAvailabilityPerCurrentMonthForSpecificClassTest()
        {
            SetValues();

            // Id of the class for which you want to display the result
            int classId = 3;

            Assert.IsNotNull(Service.StudentsAvailabilityPerCurrentMonthForSpecificClass(classId));
        }

        [TestMethod]
        public void StudentsAvailabilityPerPeriodTest()
        {
            SetValues();

            DateTime startDate = new DateTime(2021, 7, 12);
            DateTime endDate = new DateTime(2021, 7, 14);

            Assert.IsNotNull(Service.StudentsAvailabilityPerPeriod(startDate, endDate));
        }

        [TestMethod]
        public void Top3StudentsAvailabilityTest()
        {
            SetValues();

            Assert.IsNotNull(Service.Top3StudentsAvailability());
        }

        [TestMethod]
        public void Worst3StudentsAvailabilityTest()
        {
            SetValues();

            Assert.IsNotNull(Service.Worst3StudentsAvailability());
        }

        public void SetValues()
        {
            // create student records

            List<Students> sList = new List<Students>
            {
                new Students { Id = 1, FirstName = "Oleg", LastName = "Matvienko", ClassId = 1 },
                new Students { Id = 2, FirstName = "Natalia", LastName = "Prylucka", ClassId = 1 },
                new Students { Id = 3, FirstName = "Iryna", LastName = "Shevchenko", ClassId = 1 },
                new Students { Id = 4, FirstName = "Oleksandr", LastName = "Hudkovskyj", ClassId = 2 },
                new Students { Id = 5, FirstName = "Maryna", LastName = "Strilecka", ClassId = 2 },
                new Students { Id = 6, FirstName = "Ivan", LastName = "Krykunskyj", ClassId = 3 },
                new Students { Id = 7, FirstName = "Oleksandr", LastName = "Klapenkov", ClassId = 3 },
                new Students { Id = 8, FirstName = "Valentyna", LastName = "Nizenecka", ClassId = 3 }
            };

            Service.Students = sList.ToArray();

            // create class records

            List<Classes> cList = new List<Classes>
            {
                new Classes { Id = 1, Name = "Class А" },
                new Classes { Id = 2, Name = "Class B" },
                new Classes { Id = 3, Name = "Class C" }
            };

            Service.Classes = cList.ToArray();

            // create availability records for three days

            List<Availability> aList = new List<Availability>
            {
                new Availability { Id = 1, StudentId = 1, Date = new DateTime(2021, 7, 12), Available = true },
                new Availability { Id = 2, StudentId = 1, Date = new DateTime(2021, 7, 13), Available = true },
                new Availability { Id = 3, StudentId = 1, Date = new DateTime(2021, 7, 14), Available = false },
                new Availability { Id = 4, StudentId = 2, Date = new DateTime(2021, 7, 12), Available = true },
                new Availability { Id = 5, StudentId = 2, Date = new DateTime(2021, 7, 13), Available = false },
                new Availability { Id = 6, StudentId = 2, Date = new DateTime(2021, 7, 14), Available = false },
                new Availability { Id = 7, StudentId = 3, Date = new DateTime(2021, 7, 12), Available = false },
                new Availability { Id = 8, StudentId = 3, Date = new DateTime(2021, 7, 13), Available = false },
                new Availability { Id = 9, StudentId = 3, Date = new DateTime(2021, 7, 14), Available = false },
                new Availability { Id = 10, StudentId = 4, Date = new DateTime(2021, 7, 12), Available = true },
                new Availability { Id = 11, StudentId = 4, Date = new DateTime(2021, 7, 13), Available = false },
                new Availability { Id = 12, StudentId = 4, Date = new DateTime(2021, 7, 14), Available = true },
                new Availability { Id = 13, StudentId = 5, Date = new DateTime(2021, 7, 12), Available = true },
                new Availability { Id = 14, StudentId = 5, Date = new DateTime(2021, 7, 13), Available = true },
                new Availability { Id = 15, StudentId = 5, Date = new DateTime(2021, 7, 14), Available = true },
                new Availability { Id = 16, StudentId = 6, Date = new DateTime(2021, 7, 12), Available = false },
                new Availability { Id = 17, StudentId = 6, Date = new DateTime(2021, 7, 13), Available = false },
                new Availability { Id = 18, StudentId = 6, Date = new DateTime(2021, 7, 14), Available = true },
                new Availability { Id = 19, StudentId = 7, Date = new DateTime(2021, 7, 12), Available = false },
                new Availability { Id = 20, StudentId = 7, Date = new DateTime(2021, 7, 13), Available = false },
                new Availability { Id = 21, StudentId = 7, Date = new DateTime(2021, 7, 14), Available = false },
                new Availability { Id = 22, StudentId = 8, Date = new DateTime(2021, 7, 12), Available = true },
                new Availability { Id = 23, StudentId = 8, Date = new DateTime(2021, 7, 13), Available = true },
                new Availability { Id = 24, StudentId = 8, Date = new DateTime(2021, 7, 14), Available = true }
            };

            Service.Availability = aList.ToArray();
        }
    }
}
