﻿using System;
using System.Reflection;
using System.Runtime.InteropServices;

// Общие сведения об этой сборке предоставляются следующим набором
// набора атрибутов. Измените значения этих атрибутов, чтобы изменить сведения,
// связанные с этой сборкой.

// Проверьте значения атрибутов сборки

[assembly: AssemblyTitle("StudentsAvilability.Services")]
[assembly: AssemblyDescription("")]
[assembly: AssemblyCompany("")]
[assembly: AssemblyProduct("StudentsAvilability.Services")]
[assembly: AssemblyCopyright("Copyright ©  2021")]
[assembly: AssemblyTrademark("")]
[assembly: ComVisible(false)]

// Указанный ниже идентификатор GUID предназначен для идентификации библиотеки типов, если этот проект будет видимым для COM-объектов
[assembly: Guid("35b838d5-de7a-4622-9ba9-1c29ed9c4bc3")]

// Сведения о версии сборки состоят из указанных ниже четырех значений:
// 
// Основной номер версии
// Дополнительный номер версии
// Номер сборки
// Редакция
// 
// Можно задать все значения или принять номера сборки и редакции по умолчанию 
// используя "*", как показано ниже:
// <Assembly: AssemblyVersion("1.0.*")>

[assembly: AssemblyVersion("1.0.0.0")]
[assembly: AssemblyFileVersion("1.0.0.0")]
