﻿
namespace StudentsAvilability.Services
{
    public class StudentAvailable
    {
        public Students Student { get; set; }
        public int AvailableAmount { get; set; }
    }
}