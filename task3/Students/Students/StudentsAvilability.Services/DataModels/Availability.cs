﻿using System;

namespace StudentsAvilability.Services
{
    public class Availability
    {
        public int Id { get; set; }
        public int StudentId { get; set; }
        public DateTime Date { get; set; }
        public bool Available { get; set; }
    }
}