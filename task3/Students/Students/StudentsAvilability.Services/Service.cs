﻿using System;
using System.Collections.Generic;
using Microsoft.VisualBasic;

namespace StudentsAvilability.Services
{
    public static class Service
    {
        public static Students[] Students;
        public static Classes[] Classes;
        public static Availability[] Availability;

        private struct StudentAvailable
        {
            public Students Student;
            public int AvailableAmount;
        }

        // calculating average students availability per current month for all classes
        public static int StudentsAvailabilityPerCurrentMonthForAllClasses()
        {
            var studentsAverageAvailability = new List<int>();
            var studentsAvailability = new List<int>();
            var studentAvailableAmount = new List<int>();
            foreach (Availability availability in Availability)
            {
                Students student = FindStudent(availability.StudentId);

                if (availability.Date.Year == DateAndTime.Now.Year & availability.Date.Month == DateAndTime.Now.Month)
                {                    
                    if (availability.Available == true)
                    {
                        studentAvailableAmount[i] += 1;
                    }
                    else
                    {
                        studentId.Add(student.Id);
                        if (availability.Available == true)
                            studentAvailableAmount.Add(1);
                        else
                            studentAvailableAmount.Add(0);
                    }
                }
            }

            int totalAvailableCount = 0;
            foreach (int saa in studentAvailableAmount)
                totalAvailableCount += saa;

            // displaying the total number of student availibility
            return totalAvailableCount / DateTime.DaysInMonth(DateAndTime.Now.Year, DateAndTime.Now.Month);

        }

        // calculating average students availability per current month for a specific class
        public static int StudentsAvailabilityPerCurrentMonthForSpecificClass(int ClassId)
        {
            var d = DateAndTime.Now;
            int currentYear = d.Year;
            int currentMonth = d.Month;
            int daysInMonth = DateTime.DaysInMonth(currentYear, currentMonth);
            var studentId = new List<int>();
            var studentAvailableAmount = new List<int>();
            Students s;
            int i;
            foreach (Availability a in Availability)
            {
                if (a.Date.Year == currentYear & a.Date.Month == currentMonth)
                {
                    s = FindStudent(a.StudentId);
                    if (s.ClassId == ClassId)
                    {
                        i = studentId.IndexOf(s.Id);
                        if (i >= 0)
                        {
                            if (a.Available == true)
                                studentAvailableAmount[i] += 1;
                        }
                        else
                        {
                            studentId.Add(s.Id);
                            if (a.Available == true)
                                studentAvailableAmount.Add(1);
                            else
                                studentAvailableAmount.Add(0);
                        }
                    }
                }
            }

            int totalAvailableCount = 0;
            foreach (int saa in studentAvailableAmount)
                totalAvailableCount += saa;

            // displaying the total number of student availibility
            return totalAvailableCount / daysInMonth;

        }

        // calculate average students availability per period
        public static int StudentsAvailabilityPerPeriod(DateTime StartDate, DateTime EndDate)
        {
            int days = (int)DateAndTime.DateDiff(DateInterval.Day, StartDate, EndDate);
            var studentId = new List<int>();
            var studentAvailableAmount = new List<int>();
            Students s;
            int i;
            foreach (Availability a in Availability)
            {
                if (a.Date >= StartDate & a.Date <= EndDate)
                {
                    s = FindStudent(a.StudentId);
                    i = studentId.IndexOf(s.Id);
                    if (i >= 0)
                    {
                        if (a.Available == true)
                            studentAvailableAmount[i] += 1;
                    }
                    else
                    {
                        studentId.Add(s.Id);
                        if (a.Available == true)
                            studentAvailableAmount.Add(1);
                        else
                            studentAvailableAmount.Add(0);
                    }
                }
            }

            int totalAvailableCount = 0;
            foreach (int saa in studentAvailableAmount)
                totalAvailableCount += saa;

            // displaying the total number of student availibility
            return totalAvailableCount;

            

        }

        // calculating the top 3 students by availability for each class
        public static Students[] Top3StudentsAvailability()
        {
            var studentId = new List<int>();
            var studentAvailable = new List<StudentAvailable>();
            Students s;
            int i;
            StudentAvailable sa;
            var students = new List<Students>();
            foreach (Availability a in Availability)
            {
                s = FindStudent(a.StudentId);
                i = studentId.IndexOf(s.Id);
                if (i >= 0)
                {
                    if (a.Available == true)
                    {
                        sa = studentAvailable[i];
                        sa.AvailableAmount += 1;
                        studentAvailable[i] = sa;
                    }
                }
                else
                {
                    studentId.Add(s.Id);
                    sa.Student = s;
                    if (a.Available == true)
                        sa.AvailableAmount = 1;
                    else
                        sa.AvailableAmount = 0;
                    studentAvailable.Add(sa);
                }
            }

            studentAvailable.Sort((s1, s2) => s1.AvailableAmount.CompareTo(s2.AvailableAmount));
            var loopTo = studentAvailable.Count - 3;
            for (i = studentAvailable.Count - 1; i >= loopTo; i -= 1)
                students.Add(studentAvailable[i].Student);
            return students.ToArray();
        }

        // calculate the worst 3 students by the availability in the school
        public static Students[] Worst3StudentsAvailability()
        {
            var studentId = new List<int>();
            var studentAvailable = new List<StudentAvailable>();
            Students s;
            int i;
            StudentAvailable sa;
            var students = new List<Students>();
            foreach (Availability a in Availability)
            {
                s = FindStudent(a.StudentId);
                i = studentId.IndexOf(s.Id);
                if (i >= 0)
                {
                    if (a.Available == true)
                    {
                        sa = studentAvailable[i];
                        sa.AvailableAmount += 1;
                        studentAvailable[i] = sa;
                    }
                }
                else
                {
                    studentId.Add(s.Id);
                    sa.Student = s;
                    if (a.Available == true)
                        sa.AvailableAmount = 1;
                    else
                        sa.AvailableAmount = 0;
                    studentAvailable.Add(sa);
                }
            }

            studentAvailable.Sort((s1, s2) => s1.AvailableAmount.CompareTo(s2.AvailableAmount));
            for (i = 0; i <= 2; i++)
                students.Add(studentAvailable[i].Student);
            return students.ToArray();
        }

        private static Students FindStudent(int StudentId)
        {
            foreach (Students student in Students)
            {
                if (student.Id == StudentId)
                {
                    return student;
                }
            }
            return null;
        }
    }
}