﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace StudentsAvilability.Data.DataModels
{
    public class Availability
    {
        public int Id { get; set; }
        public int StudentId { get; set; }
        public DateTime Date { get; set; }
        public bool Available { get; set; }
    }
}