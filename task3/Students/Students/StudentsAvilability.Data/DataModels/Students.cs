﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace StudentsAvilability.Data.DataModels
{
    public class Students
    {
        public int Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public int ClassId { get; set; }

    }
}
