﻿using System;
using System.Data.Entity;
using System.Linq;

namespace StudentsAvilability.Data.DataModels
{
    public class StudentsContext : DbContext
    {
        public StudentsContext()
            : base("name=StudentsContext")
        {
        }

        public DbSet<Students> Students { get; set; }
        public DbSet<Classes> Classes { get; set; }
        public DbSet<Availability> Availability { get; set; }
    } 
}