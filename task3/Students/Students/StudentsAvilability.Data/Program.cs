﻿using StudentsAvilability.Data.DataModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace StudentsAvilability.Data
{
    class Program
    {
        static void Main(string[] args)
        {
            // creating a database and adding test records to it

            using (StudentsContext db = new StudentsContext())
            {
                db.Database.Delete();

                // creating students records

                Students student1 = new Students { FirstName = "Oleg", LastName = "Matvienko", ClassId = 1 };
                Students student2 = new Students { FirstName = "Natalia", LastName = "Prylucka", ClassId = 1 };
                Students student3 = new Students { FirstName = "Iryna", LastName = "Shevchenko", ClassId = 1 };
                Students student4 = new Students { FirstName = "Oleksandr", LastName = "Hudkovskyj", ClassId = 2 };
                Students student5 = new Students { FirstName = "Maryna", LastName = "Strilecka", ClassId = 2 };
                Students student6 = new Students { FirstName = "Ivan", LastName = "Krykunskyj", ClassId = 3 };
                Students student7 = new Students { FirstName = "Oleksandr", LastName = "Klapenkov", ClassId = 3 };
                Students student8 = new Students { FirstName = "Valentyna", LastName = "Nizenecka", ClassId = 3 };

                // adding students records to the database

                db.Students.Add(student1);
                db.Students.Add(student2);
                db.Students.Add(student3);
                db.Students.Add(student4);
                db.Students.Add(student5);
                db.Students.Add(student6);
                db.Students.Add(student7);
                db.Students.Add(student8);

                // creating class records

                Classes class1 = new Classes { Name = "Class А" };
                Classes class2 = new Classes { Name = "Class B" };
                Classes class3 = new Classes { Name = "Class C" };

                // adding them to the database

                db.Classes.Add(class1);
                db.Classes.Add(class2);
                db.Classes.Add(class3);

                // creating availability records for three days

                Availability availability1 = new Availability { StudentId = 1, Date = new DateTime(2021, 7, 12), Available = true };
                Availability availability2 = new Availability { StudentId = 1, Date = new DateTime(2021, 7, 13), Available = true };
                Availability availability3 = new Availability { StudentId = 1, Date = new DateTime(2021, 7, 14), Available = false };
                Availability availability4 = new Availability { StudentId = 2, Date = new DateTime(2021, 7, 12), Available = true };
                Availability availability5 = new Availability { StudentId = 2, Date = new DateTime(2021, 7, 13), Available = false };
                Availability availability6 = new Availability { StudentId = 2, Date = new DateTime(2021, 7, 14), Available = false };
                Availability availability7 = new Availability { StudentId = 3, Date = new DateTime(2021, 7, 12), Available = false };
                Availability availability8 = new Availability { StudentId = 3, Date = new DateTime(2021, 7, 13), Available = false };
                Availability availability9 = new Availability { StudentId = 3, Date = new DateTime(2021, 7, 14), Available = false };
                Availability availability10 = new Availability { StudentId = 4, Date = new DateTime(2021, 7, 12), Available = true };
                Availability availability11 = new Availability { StudentId = 4, Date = new DateTime(2021, 7, 13), Available = false };
                Availability availability12 = new Availability { StudentId = 4, Date = new DateTime(2021, 7, 14), Available = true };
                Availability availability13 = new Availability { StudentId = 5, Date = new DateTime(2021, 7, 12), Available = true };
                Availability availability14 = new Availability { StudentId = 5, Date = new DateTime(2021, 7, 13), Available = true };
                Availability availability15 = new Availability { StudentId = 5, Date = new DateTime(2021, 7, 14), Available = true };
                Availability availability16 = new Availability { StudentId = 6, Date = new DateTime(2021, 7, 12), Available = false };
                Availability availability17 = new Availability { StudentId = 6, Date = new DateTime(2021, 7, 13), Available = false };
                Availability availability18 = new Availability { StudentId = 6, Date = new DateTime(2021, 7, 14), Available = true };
                Availability availability19 = new Availability { StudentId = 7, Date = new DateTime(2021, 7, 12), Available = false };
                Availability availability20 = new Availability { StudentId = 7, Date = new DateTime(2021, 7, 13), Available = false };
                Availability availability21 = new Availability { StudentId = 7, Date = new DateTime(2021, 7, 14), Available = false };
                Availability availability22 = new Availability { StudentId = 8, Date = new DateTime(2021, 7, 12), Available = true };
                Availability availability23 = new Availability { StudentId = 8, Date = new DateTime(2021, 7, 13), Available = true };
                Availability availability24 = new Availability { StudentId = 8, Date = new DateTime(2021, 7, 14), Available = true };

                // adding them to the database

                db.Availability.Add(availability1);
                db.Availability.Add(availability2);
                db.Availability.Add(availability3);
                db.Availability.Add(availability4);
                db.Availability.Add(availability5);
                db.Availability.Add(availability6);
                db.Availability.Add(availability7);
                db.Availability.Add(availability8);
                db.Availability.Add(availability9);
                db.Availability.Add(availability10);
                db.Availability.Add(availability11);
                db.Availability.Add(availability12);
                db.Availability.Add(availability13);
                db.Availability.Add(availability14);
                db.Availability.Add(availability15);
                db.Availability.Add(availability16);
                db.Availability.Add(availability17);
                db.Availability.Add(availability18);
                db.Availability.Add(availability19);
                db.Availability.Add(availability20);
                db.Availability.Add(availability21);
                db.Availability.Add(availability22);
                db.Availability.Add(availability23);
                db.Availability.Add(availability24);

                db.SaveChanges();

                /*Console.WriteLine("Objects saved successfully");
                Console.WriteLine();*/

                List<Services.Students> studentsList = new List<Services.Students>();
                List<Services.Classes> classesList = new List<Services.Classes>();
                List<Services.Availability> availabitiesList = new List<Services.Availability>();

                // getting objects from the Students table and outputting to the console
                /*Console.WriteLine("List of objects in the Students table");
                Console.WriteLine();*/
                foreach (Students s in db.Students)
                {
                    studentsList.Add(new Services.Students { Id = s.Id, FirstName = s.FirstName, LastName = s.LastName, ClassId = s.ClassId });
                    // Console.WriteLine("Id={0} {1} {2} ClassId={3}", s.Id, s.FirstName, s.LastName, s.ClassId);
                }

                // getting objects from the Classes table and printing to the console
                /*Console.WriteLine("List of objects in the Classes table:");
                Console.WriteLine();*/
                foreach (Classes c in db.Classes)
                {
                    classesList.Add(new Services.Classes { Id = c.Id, Name = c.Name });

                    //Console.WriteLine("Id={0} {1}", c.Id, c.Name);
                }

                // getting objects from the Availability table and displaying them on the console
                /*Console.WriteLine("List of Availability table objects:");
                Console.WriteLine();*/
                foreach (Availability a in db.Availability)
                {
                    availabitiesList.Add(new Services.Availability { Id = a.Id, StudentId = a.StudentId, Date = a.Date, Available = a.Available });

                    // Console.WriteLine("Id={0} StudentId={1} {2} Available={3}", a.Id, a.StudentId, a.Date.ToString("dd.MM.yyyy"), a.Available);
                }

                // calculating and outputting their results to the console
                Services.Service.Students = studentsList.ToArray();
                Services.Service.Classes = classesList.ToArray();
                Services.Service.Availability = availabitiesList.ToArray();


                Console.WriteLine("Count of Students: {0}", studentsList.Count);
                Console.WriteLine("Count of Classes: {0}", classesList.Count);
                Console.WriteLine("Count of Availabilities: {0} \n", availabitiesList.Count);

                foreach(var a in Services.Service.Availability)
                {
                    Console.WriteLine(a);
                }
                // student average availability for the current month for all classes
                Console.WriteLine("Average student attendance per month for all classes, days: {0}", Services.Service.StudentsAvailabilityPerCurrentMonthForAllClasses());

                // student average availability for the current month for a specific class

                // Id of the class for which we need to display a result
                int classId = 3; 

                var cls = db.Classes.Find(classId);

                if (cls == null)
                {
                    Console.WriteLine("Class not found");
                }
                else
                {
                    Console.WriteLine("Average student availability per month for {0} days: {1}", cls.Name, Services.Service.StudentsAvailabilityPerCurrentMonthForSpecificClass(classId));
                }

                // average student availability over the period

                // period for which you want to get the result
                DateTime startDate = new DateTime(2021, 7, 12);
                DateTime endDate = new DateTime(2021, 7, 14);

                Console.WriteLine("Average student availability for period, days: {0}", Services.Service.StudentsAvailabilityPerPeriod(startDate, endDate));

                Console.WriteLine();

                // top 3 students by availability for each class

                Services.Students[] ss = Services.Service.Top3StudentsAvailability();

                Console.WriteLine("The three best students in attendance for each class:");
                Console.WriteLine();

                foreach (Services.Students s in ss)
                {
                    Console.WriteLine("Id={0} {1} {2} ClassId={3}", s.Id, s.FirstName, s.LastName, s.ClassId);
                }

                Console.WriteLine();


                // the worst 3 students by the availability in the school

                ss = Services.Service.Worst3StudentsAvailability();

                Console.WriteLine("The three worst students in school attendance:");
                Console.WriteLine();

                foreach (Services.Students s in ss)
                {
                    Console.WriteLine("Id={0} {1} {2} ClassId={3}", s.Id, s.FirstName, s.LastName, s.ClassId);
                }

                Console.WriteLine();

            }

            Console.Read();
        }
    }
}
