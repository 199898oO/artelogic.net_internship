﻿using System;
using System.Reflection;
using System.Runtime.InteropServices;

// Общие сведения об этой сборке предоставляются следующим набором
// набора атрибутов. Измените значения этих атрибутов, чтобы изменить сведения,
// связанные со сборкой.

// Проверьте значения атрибутов сборки

[assembly: AssemblyTitle("ScanFolders")]
[assembly: AssemblyDescription("")]
[assembly: AssemblyCompany("")]
[assembly: AssemblyProduct("ScanFolders")]
[assembly: AssemblyCopyright("Copyright ©  2021")]
[assembly: AssemblyTrademark("")]
[assembly: ComVisible(false)]

// Следующий GUID служит для идентификации библиотеки типов, если этот проект будет видимым для COM
[assembly: Guid("d97a8bee-2937-4c2c-861c-86d641a398c9")]

// Сведения о версии сборки состоят из следующих четырех значений:
// 
// Основной номер версии
// Дополнительный номер версии
// Номер сборки
// Редакция
// 
// Можно задать все значения или принять номера сборки и редакции по умолчанию 
// используя "*", как показано ниже:
// <Assembly: AssemblyVersion("1.0.*")>

[assembly: AssemblyVersion("1.0.0.0")]
[assembly: AssemblyFileVersion("1.0.0.0")]
