﻿using System;
using System.IO;

namespace ScanFolders
{
    public class MainClass
    {
        public const string fileName = "dummy.txt";
        public const string Format = "dd.MM.yyyy HH:mm:ss";
        public static string DateNow = DateTime.Now.ToString(Format);

        public static void Main(string[] args)
        {
            //якщо не вказаний аргумент командної строки при запуску програми, видає повідомлення про помилку
            if (args.Length == 0)
            {

                Console.WriteLine("Error : Specify folder for the script!\n");

            }
            //приймаємо директорію на вхід
            var root = new DirectoryInfo(args[0]).ToString();

            // шукаємо рекурсивно директорії
            string[] directories = Directory.GetDirectories(root, "", SearchOption.AllDirectories);

            //запис файла в кожній із знайдених рекурсивно директорії
            foreach (string directory in directories)
            {
                DirectoryInfo DirectoryInfoFile = new DirectoryInfo(directory);
                WriteContent(DirectoryInfoFile, DateNow);


            }
        }
        public static void WriteContent(DirectoryInfo root, string datetime)
        {
            // вивести в консоль повний шлях поточної папки
            Console.WriteLine(root.FullName);
            try
            {
                // визначаємо потік для виводу тексту в файл
                StreamWriter writer;

                //формуємо повний шлях до папки,  включаючи імя файлу
                string fName = root.FullName + @"\" + fileName;
                if (File.Exists(fName) == false)
                {
                    //якщо такий файл відсутній, то створюємо його для добавлення тексту
                    writer = File.CreateText(fName);
                }
                else
                {
                    //якщо такий файл присутній, то відкриваємо його для добавлення тексту
                    writer = File.AppendText(fName);
                }

                //записуємо дату і час в файл
                writer.WriteLine(DateTime.Now.ToString("dd.MM.yyyy HH:mm:ss"));


                writer.Close();
                writer.Dispose();
            }
            catch (Exception ex)
            {
                //якщо є помилка видаємо в консоль повідомлення про неї
                Console.WriteLine("\n", ex.Message, "\n");
            }
        }
    }
}
