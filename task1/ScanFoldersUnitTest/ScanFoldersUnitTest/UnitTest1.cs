using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.IO;
using System.Linq;
using static ScanFolders.MainClass; 

namespace ScanFoldersUnitTest
{
    [TestClass]
    public class UnitTest1
    {

        private static DirectoryInfo root = new DirectoryInfo(@"D:\\Test");
        private const string Format = "dd.MM.yyyy HH:mm:ss";
        private const string fileName = "dummy.txt";
        private static string DateNow = DateTime.Now.ToString(Format);
        private static string fName = root.FullName + @"\" + fileName;
        


        [TestMethod]
        public void TestMethod1()
        {
            WriteContent(root, DateNow);
            var lastline = File.ReadAllLines(fName).Last();
            Assert.AreEqual(expected:DateNow,actual:lastline);
        }
    }
}
