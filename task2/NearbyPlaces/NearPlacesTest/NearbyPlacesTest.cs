using Microsoft.VisualStudio.TestTools.UnitTesting;
using NearbyPlaces;
using System.Collections.Generic;
using System.IO;
using System.Net;

namespace NearPlacesTest
{
    [TestClass]

    public class NearbyPlacesTest
    {
        private const string TestURL = "https://maps.googleapis.com/maps/api/place/nearbysearch/json?location=49.2505198,23.8557903&radius=5000&key=AIzaSyCEa34HYzwUOvRdXUKdAv3K7rgOwWGHazE&rankBy=distance&type=point_of_interest&keyword=cruise";
        public static NearLocation testlocation;
        //creating an object for testing
        [ClassInitialize]
        public static void InitializingClass(TestContext context)
        {
            testlocation = new NearLocation();
        }

        [TestMethod]
        public void SetLocationTest()
        {
            testlocation.SetLocation("49.2505198,23.8557903");

            //setting a location for test class
            Assert.AreEqual("49.2505198,23.8557903", testlocation.location, "Can't save a location.");
        }

        [TestMethod]
        public void SetRadiusTest()
        {
            testlocation.SetRadius(5000);

            Assert.AreEqual(5000, testlocation.radius, "Can't save a radius.");
        }


        [TestMethod]
        public void NewUrlTest()
        {
            testlocation.NewUrl("https://maps.googleapis.com/maps/api/place/nearbysearch/json?" +
                    "location=49.2505198,23.8557903"+
                    "&radius=5000"+
                    "&key=AIzaSyCEa34HYzwUOvRdXUKdAv3K7rgOwWGHazE" +
                    "&rankBy=distance&type=point_of_interest&keyword=cruise");

            Assert.AreEqual(TestURL, testlocation.apiUrl, "URLs are not match.");
        }

        [TestMethod]
        public void ParseJsonTest()
        {
            string json_example = @"{
                  CPU: 'Intel',
                  Drives: [
                    'DVD read/writer',
                    '500 gigabyte hard drive'
                  ]
                }";
            testlocation.ParseJson(json_example);

            Assert.AreNotEqual(null, testlocation.obj, "JSON object not NULL.");
        }
        //testing WebRequest
        [TestMethod]
        public void GetDataTest()
        {
            HttpWebRequest request = WebRequest.Create(TestURL) as HttpWebRequest;

            string jsonValue = "";
            using (HttpWebResponse response = request.GetResponse() as HttpWebResponse)
            {
                StreamReader reader = new StreamReader(response.GetResponseStream());
                jsonValue = reader.ReadToEnd();
            }

            Assert.AreNotEqual("", jsonValue, "Can't load a data from URL via HttpWebRequest");
        }
    }
}
